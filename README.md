# Team Fortress 2

This is a TF2 docker image based upon Debian:Bullseye and https://gitlab.com/cmunroe/steamcmd.

## Start Server

`docker run --name Team-Fortress-2 -d -it --restart always -p 27015:27015 -p 27015:27015/udp registry.gitlab.com/cmunroe/tf2-thin:latest`

## Helpful Commands

### Interface with console

`docker attach Team-Fortress-2`

To leave console, you need to press CTRL + P -> Q.


## Environment Variables

Defaults:

```
APPDIR=/opt
APPID=232250
TF2_PORT=27015
TF2_MAXPLAYERS=24
TF2_MAP=ctf_2fort
TF2_IP=0.0.0.0
CACHE_BUILD=false
CACHE=false
```

#### APPID

Shouldn't be changed, this is what sets us to download Team Fortress 2.

#### APPDIR

This sets the install location for Team Fortress 2. 

#### TF2_PORT

This sets the port for Team Fortress 2 to listen on.

#### TF2_HOSTNAME

This will look inside /opt/tf/cfg/server.cfg for a hostname element and change it to what you set in the variable.

#### TF2_MAXPLAYERS

This sets the maximum number of players on Team Fortress 2.

#### TF2_MAP

This sets the map for Team Fortress 2.

#### TF2_IP

This sets the IP address for Team Fortress 2 to listen on.

#### CACHE_BUILD 

This will download a copy of Team Fortress 2 to the /cache folder.

#### CACHE

This will use the cache folder to quickly build a Team Fortress 2 instance without needing to download the entire server.

## Caching

If you run multiple TF2 instances or if you have a slow internet; You would likely want to use a cache.

### Cache Setup
```
# Create a static tf2-cache.
docker create --name tf2-cache -v tf2-cache:/cache -e CACHE_BUILD=true registry.gitlab.com/cmunroe/tf2-thin

# Set Volume Permissions
docker run --rm --volumes-from=tf2-cache debian chown -R 1000:1000 /cache

# Download to cache.
docker start -a tf2-cache

# Automate update of cache via cron
0 0 * * 1 /usr/bin/docker start -a tf2-cache

```

### Update Cache

`docker start -a tf2-cache`

### Getting your containers to pull from cache.

Docker-compose:

```
version: '3.3'
services:
    dodgeball:
        network_mode: host
        container_name: Team-Fortress-2
        restart: always
        stdin_open: true
        tty: true
        environment:
            - TF2_IP=x.x.x.x
            - CACHE=true
        image: 'registry.gitlab.com/cmunroe/tf2-thin'
        volumes:
            - tf2-cache:/cache:ro

volumes:
    tf2-cache:
      external: true
```


## Other Projects / References

https://github.com/spiretf/docker-comp-server

https://github.com/spiretf/docker-tf2-server

https://github.com/CM2Walki/TF2

https://github.com/GameServers/TeamFortress2

https://hub.docker.com/r/cm2network/tf2

https://hub.docker.com/r/gameservers/teamfortress2

