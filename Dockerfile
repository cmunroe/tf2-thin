FROM registry.gitlab.com/cmunroe/steamcmd:latest

WORKDIR /opt/
COPY /scripts /scripts

ENV APPDIR=/opt
ENV APPID=232250
ENV TF2_PORT=27015
ENV TF2_MAXPLAYERS=24
ENV TF2_MAP=ctf_2fort
ENV TF2_IP=0.0.0.0
ENV CACHE_BUILD=false
ENV CACHE=false

EXPOSE ${TF2_PORT}
EXPOSE ${TF2_PORT}/udp
EXPOSE 27021/tcp
EXPOSE 27020/udp

RUN useradd -u 1000 -m tf2 && \
    apt-get install wget lib32z1 libncurses5:i386 libbz2-1.0:i386 lib32gcc-s1 lib32stdc++6 libtinfo5:i386 libcurl3-gnutls:i386 libsdl2-2.0-0:i386 -y  && \ 
    chmod +x /scripts/* && \
    mkdir -p /opt/tf/addons && \
    chown -R tf2:tf2 /opt && \
    mkdir /cache && \
    chown -R tf2:tf2 /cache

USER tf2

RUN installAddons

CMD startGameServer
